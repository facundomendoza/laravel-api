# Instalación y configuración
Para este proyecto, se asume que el participante de la capacitación tiene una base de datos creada y configurada, y tiene conocimientos básicos de Git.

Para probar los endpoints que desarrollemos utilizaremos [Postman](https://www.postman.com/).

## 1. Instalar librerías necesarias
Para este proyecto voy a estar utilizando una distribución Ubuntu/Linux Mint ya que nos permite armar un entorno de desarrollo con unos pocos comandos en la Terminal. Las versiones que utilizo yo son Apache 2.4 con PHP 8 y MySQL 8.

	$ sudo add-apt-repository ppa:ondrej/php
	$ sudo add-apt-repository ppa:ondrej/apache2
	$ sudo apt-get update
	$ sudo apt-get install apache2 mysql-client php php-mysql libapache2-mod-php phpmyadmin php-zip php-xml php-gd php-mbstring php-sodium composer git

Para Windows, recomiendo instalar Laragon, dejo una lista de videos que les puede servir:
1. [Instalar Laragon en Windows](https://www.youtube.com/watch?v=csKUkTnbTAA)
2. [Como Actualizar PHP 8.0 en Laragon](https://www.youtube.com/watch?v=csKUkTnbTAA)
3. [Como instalar Git en Windows 10](https://www.youtube.com/watch?v=dbEqN7EY-Mg)

Si utilizamos Laragon, y actualizamos PHP a la versión 8, para poder habilitar las extensiones de PHP, es necesario modificar el archivo `php.ini` que se encuentra dentro de la carpeta de la versión de PHP que se extrajo en `laragon/bin/php` y des-comentar las siguientes extensiones:

1. gd
2. fileinfo
3. curl
4. mbstring
5. mysqli
6. openssl
7. pdo_mysql
8. sodium

## 2. Clonar el proyecto
Utilizaremos Git para clonar este proyecto, si usamos Laragon, es necesario clonarlo en la carpeta `www` de Laragon.

	git clone https://gitlab.com/facundomendoza/laravel-api.git

## 3. Instalar las librerías de Laravel
Cuando se sube un proyecto de Laravel a Git, se ignora la carpeta `vendor` del proyecto para ahorrar espacio. Esta guarda las librerías de Laravel y son importantes para su funcionamiento, y se pueden descargar mediante Composer. Para ello, necesitamos entrar en la carpeta del proyecto con la terminal y escribir el siguiente comando:

	composer install

## 4. Configuración del proyecto
Una vez instaladas las librerías que va a utilizar Laravel, hacemos una copia del archivo `.env.example` **(con cuidado de no borrarlo)** y la nombramos `.env`, este sera nuestro archivo de configuración del entorno, que solo estará en nuestras computadoras y no se sube a Git tampoco. Aquí configuraremos el acceso a la base de datos modificando las siguientes lineas:

	DB_DATABASE=laravel #Nombre de la base de datos
	DB_USERNAME=root #Nombre de usuario de la base de datos
	DB_PASSWORD= #Contraseña del usuario de la base de datos

Laragon viene por defecto con el usuario `root` con la contraseña en blanco para su base de datos.

Por ultimo, debemos crear nuestra llave de encriptado, que se almacenara en la variable de entorno `APP_KEY`, mediante el siguiente comando:

	php artisan key:generate

Esta llave se utiliza para cifrar todos los datos que se necesiten del proyecto, así que es importante no revelarla.
## 5. Crear las tablas del proyecto
El proyecto viene con varias tablas por defecto que son importantes para la autenticación. Para crearlas, usaremos el siguiente comando:

	php artisan migrate

También viene por defecto un usuario creado, con el email `admin@example.com` y contraseña `12345678`. Para cargar este usuario en la tabla, usaremos el comando:

	php artisan db:seed
## 6. Crear las llaves de encriptado para la autenticación
La razón por la cual no creamos el proyecto desde 0 y utilizamos este repositorio, es que este proyecto viene con [Laravel Passport](https://laravel.com/docs/8.x/passport) instalado por defecto para tener una autenticación desde el principio. Pero antes de que funcione la autenticación con Passport es necesario generar las llaves de encriptado, y se puede hacer con el siguiente comando:

	php artisan passport:install

## 7. Correr el servidor del proyecto
Laravel viene con un comando para poder correr el servidor del proyecto y poder probar nuestra aplicación:

	php artisan serve

Este comando deja andando el servidor y es posible acceder desde un navegador mediante la URL `http://localhost:8000`.

En Laragon, se configuran automáticamente los Virtual Hosts en Apache cuando detecta una nueva carpeta dentro de `www` y para acceder al proyecto se hace mediante `nombre-de-la-carpeta.test`, si no se modifico el nombre de la carpeta una vez clonado desde el repositorio, la URL de acceso debería ser `laravel-api.test`.
